<?php

namespace Drupal\integer_textfield_widget\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'integer_textfield' widget.
 *
 * @FieldWidget(
 *   id = "integer_textfield",
 *   label = @Translation("Integer textfield"),
 *   field_types = {
 *     "string"
 *   }
 * )
 */
class IntegerTextfieldWidget extends StringTextfieldWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['value']['#element_validate'][] = [$this, 'elementValidate'];
    return $element;
  }

  /**
   * Validate the integer text field.
   */
  public function elementValidate($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    if (strlen($value) == 0) {
      $form_state->setValueForElement($element, '');
      return;
    }
    if (!preg_match('/^[0-9]+$/', $value)) {
      $name = empty($element['#title']) ? $element['#parents'][0] : $element['#title'];
      $form_state->setError($element, $this->t('%name is not a valid number.', ['%name' => $name]));
    }
  }

}
