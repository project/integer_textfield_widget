<?php

namespace Drupal\Tests\integer_textfield_widget\Functional;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Test Integer Textfield Widget.
 *
 * @group integer_textfield_widget
 */
class IntegerTextfieldWidgetTest extends BrowserTestBase {

  use StringTranslationTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'field',
    'node',
    'integer_textfield_widget',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->drupalCreateContentType(['type' => 'article']);
    $user = $this->drupalCreateUser([
      'create article content',
      'edit own article content',
    ]);
    $this->drupalLogin($user);
    $this->entityTypeManager = $this->container->get('entity_type.manager');

  }

  /**
   * Validate integer Textfield widget.
   */
  public function testIntegerTextfieldWidget() {
    $entity_type_id = 'node';
    $bundle = 'article';
    $field_name = 'field_serial_number';

    FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => $entity_type_id,
      'type' => 'string',
    ])->save();
    FieldConfig::create([
      'field_name' => $field_name,
      'label' => 'Serial number',
      'description' => 'Serial number description',
      'entity_type' => $entity_type_id,
      'bundle' => $bundle,
    ])->save();

    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');

    /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $form_display */
    $form_display = $entity_type_manager->getStorage('entity_form_display')
      ->load('node.article.default');

    // Add the new field to the edit form.
    $form_display->setComponent($field_name, [
      'type' => 'integer_textfield',
      'settings' => [
        'size' => 60,
        'placeholder' => '',
      ],
    ])->save();

    $session = $this->assertSession();

    // Confirm field label are rendered.
    $this->drupalGet('node/add/article');
    $session->fieldExists("field_serial_number[0][value]");
    $session->responseContains('Serial number');

    // Test basic input an invalid field.
    $title = $this->randomMachineName();
    $edit = [
      'title[0][value]' => $title,
      'field_serial_number[0][value]' => "Invalid test",
    ];

    $this->drupalPostForm(NULL, $edit, $this->t('Save'));
    $this->assertText('Serial number is not a valid number.');

    // Try the basic entry with a valid field.
    $edit = [
      'title[0][value]' => $title,
      'field_serial_number[0][value]' => "12345678987654321",
    ];

    $this->drupalPostForm(NULL, $edit, $this->t('Save'));
    $this->assertSession()->statusCodeEquals(200);
    $t_args = ['@type' => $bundle, '%title' => $title];
    $this->assertText(strip_tags($this->t('@type %title has been created.', $t_args)), 'The node was created.');

  }

}
